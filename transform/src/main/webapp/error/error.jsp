<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Error page</title>
</head>
<body>
	<c:choose>
      <c:when test="${empty error }"><h3>Oh!Error!!!</h3>
      <br />
      </c:when>

      <c:otherwise>${error}
      <br />
      </c:otherwise>
</c:choose>
	<a href="TransformerController">Home</a>
</body>
</html>
