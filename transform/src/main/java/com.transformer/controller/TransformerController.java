package com.transformer.controller;

	import java.io.IOException;
	import java.io.PrintWriter;
	import java.net.URL;
	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;
	import javax.xml.transform.Source;
	import javax.xml.transform.Transformer;
	import javax.xml.transform.TransformerFactory;
	import javax.xml.transform.stream.StreamResult;
	import javax.xml.transform.stream.StreamSource;

/**
	 * Servlet implementation class TransformerController
	 */
	@WebServlet("/TransformerController")
	public class TransformerController extends HttpServlet {
		private static final long serialVersionUID = 1L;
	public final static String FS = System.getProperty("file.separator");

	/**
		 * Default constructor.
		 */
		public TransformerController() {
			// TODO Auto-generated constructor stub
		}

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		transformer(response);
		}


		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			transformer(response);

		}
		public void transformer( HttpServletResponse response){
// Set content type for HTML.
			response.setContentType("text/html; charset=UTF-8");
			// Output goes to the response PrintWriter.
			PrintWriter out = null;
			try{
				out = response.getWriter();

				TransformerFactory tFactory =
						TransformerFactory.newInstance();
				//get the real path for xml and xsl files.
				String ctx = getServletContext().getRealPath("") + FS;
				// Get the XML input document and the stylesheet, both in the servlet
				// engine document directory.
				System.out.println("HHHHHHHHHH");
				Source xmlSource =
						new StreamSource
								(new URL("file", "", ctx+"s1.xml").openStream());
				Source xslSource =
						new StreamSource
								(new URL("file", "", ctx+"Stock.xsl").openStream());
				// Generate the transformer.
				Transformer transformer =
						tFactory.newTransformer(xslSource);
				// Perform the transformation, sending the output to the response.
				transformer.transform(xmlSource,
						new StreamResult(out));
			}
			// If an Exception occurs, return the error to the client.
			catch (Exception e)
			{
				if (out != null) {
					out.write(e.getMessage());
				}
				e.printStackTrace(out);
			}
			// Close the PrintWriter.
//			out.close();
		}
	}